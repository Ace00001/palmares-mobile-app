define([
  'app',
  'services/usercontext!'
], function (app, usercontext) {
  'use strict';
  // the run blocks
  var globalFunctions = {
    inRole: function () {
      for (var i = 0; i < arguments.length; i++) {
        var r = arguments[i];
        if (r == this.usercontext.role) {
          return true;
        }
      };
      return false;
    },
    isManager: function () {
      return this.inRole('ceo') || this.inRole('director');
    },
    userRole: function () {
      return this.usercontext.role;
    }
  };

  function run($ionicPlatform, $rootScope, $window) {
    console.log(usercontext)
    $rootScope.usercontext = usercontext;
    app.usercontext = usercontext;
    console.log(usercontext)
    $.extend($rootScope, globalFunctions);
    $.extend(app, globalFunctions);
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if ($window.cordova && $window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if ($window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });
  }

  angular.module('app').run([
    '$ionicPlatform',
    '$rootScope',
    '$window',
    run
  ]);
});
