define(['app'], function (app) {
  'use strict';

  var dependencies = ['$state'];
  angular.module('app').controller('SearchController', dependencies.concat(SearchController));

  function SearchController($state) {
    var vm = this;

    var views = ['appointment', 'sale', 'pands', 'client'];

    function inViews(view) {
      for (var i = 0; i < views.length; i++) {
        var v = views[i];
        if (v == view) {
          return true;
        }
      };
      return false;
    };

    vm.serviceStyle = function () {
      return app.inRole('technician')
    };

    vm.appointmentStyle = function () {
      return app.inRole('calloperator', 'seller', 'director', 'ceo')
    };

    vm.showSale = function () {
      return app.inRole('seller', 'director', 'ceo')
    };

    vm.showPands = function () {
      return app.inRole('technician', 'seller', 'director', 'ceo')
    };

    vm.showClients = function () {
      return app.inRole('calloperator', 'seller', 'director', 'ceo')
    };

    vm._choose = $state.params.view;

    if (!inViews(vm._choose)) {
      vm._choose = 'appointment';
    };

    vm.chosen = function (it) {
      return it == this._choose;
    };

    vm.goto = function (it) {
      vm._choose = it;
    }

  }

});
