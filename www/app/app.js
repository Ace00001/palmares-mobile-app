// The main app definition
// --> where you should load other module depdendencies
define([
  'ionic',
  'ngCordova',
  'ion-place-autocomplete',
  'chart',
  'uiValidate',
  'angular-svg-round-progressbar'
], function () {
  'use strict';

  // the app with its used plugins
  var app = angular.module('app', [
    'ionic',
    'ngCordova',
    'uiGmapgoogle-maps',
    'ion-place-tools',
    'ui.validate',
    'angular-svg-round-progressbar'
  ]);
  // return the app so you can require it in other components
  return app;
});
