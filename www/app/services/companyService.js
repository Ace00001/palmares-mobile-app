define(['./http'], function (http) {

  var Service = {
    // GET
    getCompany: function (companyId) {
      return http.get('/api/companies/' + companyId);
    },
    // PUT
  };
  return Service;
});
