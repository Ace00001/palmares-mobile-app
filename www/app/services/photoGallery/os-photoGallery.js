﻿define(['app'], function (app) {
  angular.module("app")
    .directive("osPhotoGallery", ["$rootScope", osPhotoGallery]);

  function osPhotoGallery(o) {
    return {
      restrict: 'AE',
      templateUrl: "app/services/photoGallery/os-photoGallery.html",
      bindToController: true,
      scope: {
        photos: "=",
        addphoto: "=",
        deletephoto: "=",
        sendphoto: "=",
        newphoto: "=",
        canadd: "=",
        candelete: "="
      },
      controller: ['$scope', osPhotoGalleryController],
      controllerAs: "vm"
    }
  };

  function osPhotoGalleryController($scope) {
    var vm = this;

    $scope.$on('PHOTOGALLERY_REFRESH', function () {
      e();
    })

    function e() {
      vm.images = [];
      for (var o = 0; o < vm.photos.length; o++) {
        vm.images.push({
          id: vm.photos[o].id,
          tag: vm.photos[o].tag,
          index: o,
          imageClass: "square-30",
          fullScreen: !1
        })
      }
    };

    vm.images = [],
      e(),

      $scope.$watch(function () {
        return vm.photos
      }, function () {
        e()
      }),

      vm.fullphoto = function (o) {
        vm.images[o].fullScreen = !0,
          vm.images[o].imageClass = "square-98"
      },

      vm.resetphoto = function (o) {
        vm.images[o].fullScreen = !1,
          vm.images[o].imageClass = "square-30"
      }
  };

});
