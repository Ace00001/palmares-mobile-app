define(['./http', 'common/map', 'app'],
  function (http, map, app) {

    function convertToMoment(dayOffInterval) {
      return {
        startDate: moment(dayOffInterval.startDate),
        endDate: moment(dayOffInterval.endDate)
      };
    }

    var Service = {
      _userInfo: null,
      login: function (email, password) {
        var that = this;
        return http.post('/api/account/login', {
          username: email,
          password: password
        }).then(function (r) {
          localStorage.setItem('token', r.accessToken);
          return that.authorized();
        });
      },
      authorized: function () {
        var that = this;
        var ret = $.Deferred();
        if (!localStorage.getItem('token')) {
          ret.resolve(false);
          return ret;
        }

        http.get('/api/users/userinfo').done(function (r) {
          ret.resolve(true);
        })
          .done(function (r) {
            that._userInfo = r;
            localStorage.setItem('user-info', angular.toJson(r));
            $.extend(app.usercontext, r);
          })
          .fail(function () {
            ret.resolve(false);
          });
        return ret;
      },
      logoff: function () {
        this._userInfo = null;
        localStorage.removeItem('user-info');
        localStorage.removeItem('token');
      },
      getNbYearFromCreation: function () {
        return http.get('/api/users/nbYearFromCreation');
      },
      getUser: function (userProfileId) {
        return http.get('/api/users/' + userProfileId);
      },
      getUserSet: function () {
        return http.get('/api/users/');
      },
      getByRole: function (role) {
        return http.get('/api/users/getByRole/' + role);
      },
      getDayOffSet: function (userProfileId) {
        return http.get('/api/users/getDayOffSet/' + userProfileId)
          .then(function (dayOffSet) {
            return dayOffSet.select(function (dayOff) {
              var dayOffMoment = convertToMoment(dayOff);
              return dayOffMoment;
            });
          });
      }
    };

    return Service;
  })
