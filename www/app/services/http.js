define(["app"], function (app) {
  var Service = {
    get: function (url, req) {
      app.ionicLoading.show();
      return $.ajax({
        dataType: "json",
        url: app.API_URL + url,
        data: req,
        headers: {
          Authorization: "Bearer " + localStorage.getItem('token')
        }
      })
        .always(function () {
          app.ionicLoading.hide();
        });
    },
    post: function (url, req) {
      app.ionicLoading.show();
      return $.ajax({
        type: "POST",
        dataType: "json",
        url: app.API_URL + url,
        data: req,
        headers: {
          Authorization: "Bearer " + localStorage.getItem('token')
        }
      })
        .always(function () {
          app.ionicLoading.hide();
        });
    },
    patch: function (url, req) {
      app.ionicLoading.show();
      return $.ajax({
        type: "PATCH",
        dataType: "json",
        url: app.API_URL + url,
        data: req,
        headers: {
          Authorization: "Bearer " + localStorage.getItem('token')
        }
      })
        .always(function () {
          app.ionicLoading.hide();
        });
    },
    put: function (url, req) {
      app.ionicLoading.show();
      return $.ajax({
        type: "PUT",
        dataType: "json",
        url: app.API_URL + url,
        data: req,
        headers: {
          Authorization: "Bearer " + localStorage.getItem('token')
        }
      })
        .always(function () {
          app.ionicLoading.hide();
        });
    },
    delete: function (url, req) {
      app.ionicLoading.show();
      return $.ajax({
        type: "DELETE",
        dataType: "json",
        url: app.API_URL + url,
        data: req,
        headers: {
          Authorization: "Bearer " + localStorage.getItem('token')
        }
      })
        .always(function () {
          app.ionicLoading.hide();
        });
    }
  };

  return Service;
});
