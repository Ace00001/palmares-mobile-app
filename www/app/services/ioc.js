define(["require"], function (require) {
  var map = {
    'userService': 'services/userService',
    'profileService': 'services/profileService',
    'branchService': 'services/branchService',
    'companyService': 'services/companyService'
  };

  return {
    load: function (name, req, onLoad) {
      if (name in map) {
        name = map[name];
      }

      require([name], function (Service) {
        if (typeof Service == "object") {
          onLoad(Service);
        } else {
          onLoad(new Service());
        }
      });
    }
  };
});
