define(['./http'], function (http) {

  var Service = {
    // GET
    getBranchSet: function () {
      return http.get('/api/branches');
    },
    getBranch: function (branchId) {
      return http.get('/api/branches/' + branchId);
    },
    // POST
  };
  return Service;
});
