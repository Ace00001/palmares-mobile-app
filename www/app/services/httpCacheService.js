define([], function () {
  return {
    _cache: {

    },
    resolve: function (key, createFnc) {
      if (this._cache[key]) {
        return this._cache[key];
      }
      var req = createFnc();
      this._cache[key] = req;
      return req;
    }
  };
});
