define(['./http', 'app'], function (http, app) {
  angular.module('app').factory('pictureService', ['$cordovaCamera', '$cordovaCapture', '$q',
    function ($cordovaCamera, $cordovaCapture, $q) {
      var Service = {
        takePhoto: function () {
          var options = {
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            encodingType: Camera.EncodingType.JPEG,
            quality: 50,
            targetHeight: 640,
            targetWidth: 640,
            correctOrientation: true,
            saveToPhotoAlbum: false
          };
          return $cordovaCamera.getPicture(options)
            .then(function (imageData) {
              return imageData;
            }, function (err) {
              return err;
            });
        },
        selectPhoto: function () {
          var options = {
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
            encodingType: Camera.EncodingType.JPEG,
            quality: 50,
            targetHeight: 640,
            targetWidth: 640,
            correctOrientation: true,
            saveToPhotoAlbum: false
          };
          return $cordovaCamera.getPicture(options)
            .then(function (imageData) {
              return imageData;
            }, function (err) {
              return err;
            });
        }
      };

      return Service;
    }]);
});
