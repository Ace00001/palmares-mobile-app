define(['./http', 'common/map', './httpCacheService', 'common/formatter', 'common/filtering'],
  function (http, map, cache, formatter, filtering, $filter) {
    // testSLB
    var appCreator = null;
    var months = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'];
    var Service = {
      //Retourne le palamrès / liste des utilisateurs pour le rôle et les filtres.
      //
      getList: function (role, filter) {
        // filter = {};
        // par défaut, filtre sur le mois en cours.
        var timeFilter =
          filter &&
          filter.criterias &&
          filter.criterias.single ?
          filter.criterias.single : 'month';
        //
        var sort =
          filter &&
          filter.criterias &&
          filter.criterias.sort ?
          filter.criterias.sort : null;

        var url = '/api/prizelist/';
        var time = moment().format("YYYY-MM-DD");
        var timeArr = time.split('-');
        var timeMonth = null;
        console.log(filter);
        if(filter && filter.criterias && filter.criterias.month){
          timeMonth = months.indexOf(filter.criterias.month)+1;
          if(timeMonth < 10){
            timeMonth = '0'+timeMonth;
          }    
        } 
          console.log(timeMonth)
          time = timeArr[0]+'-'+timeMonth+'-'+timeArr[2];
          // time = '2017'+'-'+'01'+'-'+timeArr[2];
        console.log(time)
        console.log(timeFilter);
        console.log(sort);
        console.log(role);
        console.log(filter);
        // time = '2017-1-08'
        // url = '/api/prizelist/caAchieved/' + time;
        // TECHNICIAN
        if (role == 'technician') {
          if (timeFilter == 'month') {
            console.log('api/prizelist/getTechnicianByMonth')
            url = '/api/prizelist/getTechnicianByMonth/' + time;
          } 
          // else if (timeFilter == 'year') {
          //   url = '/api/prizelist/getTechnicianByYear/' + time;
          // } else if (timeFilter == 'today') {
          //   url = '/api/prizelist/getTechnicianByDay/' + time;
          // } else {
          //   url = '/api/prizelist/getTechnician/';
          // }
          sort = sort ? sort : 'caAchieved';
        } else if (role == 'agence') {
          if (timeFilter == 'month') {
            console.log('api/prizelist/getBranchByMonth/');
            url = '/api/prizelist/getBranchByMonth/' + time;
          } else if (timeFilter == 'year') {
            url = '/api/prizelist/getBranchByYear/' + time;
          } else if (timeFilter == 'today') {
            url = '/api/prizelist/getBranchByDay/' + time;
          } else {
            url = '/api/prizelist/getBranch/';
          }
        } else if (role == 'director') {
          if (timeFilter == 'month') {
            console.log('api/prizelist/getDirectorByMonth/');
            url = '/api/prizelist/getDirectorByMonth/' + time;
          } else if (timeFilter == 'year') {
            url = '/api/prizelist/getDirectorByYear/' + time;
          } else if (timeFilter == 'today') {
            url = '/api/prizelist/getDirectorByDay/' + time;
          } else {
            url = '/api/prizelist/getDirector/';
          }
          sort = sort ? sort : 'caDone';
        } else if (role == 'seller') {
          if (timeFilter == 'month') {
            console.log('api/prizelist/getSellerByMonth/')
            url = '/api/prizelist/getSellerByMonth/' + time;
          } else if (timeFilter == 'year') {
            url = '/api/prizelist/getSellerByYear/' + time;
          } else if (timeFilter == 'today') {
            url = '/api/prizelist/getSellerByDay/' + time;
          } else {
            url = '/api/prizelist/getSeller/';
          }
          sort = sort ? sort : 'caDone';
        } else if (role == 'calloperator') {
          if (timeFilter == 'month') {
            console.log('api/prizelist/getCallOperatorByMonth/')
            url = '/api/prizelist/getCallOperatorByMonth/' + time;
          } else if (timeFilter == 'year') {
            url = '/api/prizelist/getCallOperatorByYear/' + time;
          } else if (timeFilter == 'today') {
            url = '/api/prizelist/getCallOperatorByDay/' + time;
          } else {
            url = '/api/prizelist/getCallOperator/';
          }
          sort = sort ? sort : 'sellerAppDone';
        } else if (role == 'directorAndSeller') { // Todo : à refactoriser, utilisé pour le choix des prospecteurs / vendeurs à la création d'un RDV
          url = '/api/users/getDirectorAndSeller/';
        } else if (role == 'directorAndSellerAndCalloperator') {
          url = '/api/users/getDirectorAndSellerAndCalloperator/';
        }

        // Récupère le
        return http.get(url).then(function (r) {
            var list = r.itemSet.select(function (it) {
              return {
                status: [1].random(),
                bigz: it.bigZ,
                fullName: it.fullName,
                firstName: it.firstName,
                lastName: it.lastName,
                id: it.id,
                photoId: it.photoId,
                sortValue: it[sort]
              }
            });
            // Tri par ordre décroissant
            return list.sort(function (i1, i2) {
              return i2.sortValue - i1.sortValue;
            });
          })
          // client-side filter
          .then(function (r) {
            if (!filter) {
              return r;
            }
            if (filter.text) {
              r = r.where(function (it) {
                return it.fullName.indexOf(filter.text) >= 0;
              });
            }

            return r;
          });
      },
      getProfile: function (id) {
        // TODO : à simplifier
        return http.get('/api/profils/getProfileRole/' + id)
          .then(function (r) {
            r.role = r.role.toLowerCase();
            r.id = id;
            if (r.role == 'agence') {
              return r;
            }
            return http.get('/api/users/' + id)
              .then(function (r1) {
                r1.role = r.role;
                return r1;
              });
          })
          .then(function (r) {
            var url = null;
            if (r.role == 'calloperator') {
              url = '/api/profils/getCallOperatorIndicatorGlobal/' + id
            } else if (r.role == 'agence') {
              url = '/api/profils/getBranchIndicatorGlobal/' + id
            } else if (r.role == 'director') {
              url = '/api/profils/getDirectorIndicatorGlobal/' + id
            } else if (r.role == 'seller') {
              url = '/api/profils/getSellerIndicatorGlobal/' + id
            } else if (r.role == 'technician') {
              url = '/api/profils/getTechnicianIndicatorGlobal/' + id
            } else {
              notSupportedException();
            }
            return http.get(url)
              .then(function (t1) {
                return {
                  userInfo: r,
                  indicator: t1,
                  role: r.role
                }
              });
          });
      },
      getProgress: function (id) {
        return this.getProfile(id)
          .then(function (profile) {
            var url = '/api/profils/getUserHeader/';
            if (profile.role == 'agence') {
              url = '/api/profils/getBranchHeader/'
            }
            return http.get(url + id).then(function (r) {
              return {
                profile: {
                  firstName: r.branchName ? r.branchName : r.firstName,
                  lastName: r.lastName,
                  photoId: r.photoId
                },
                progress: r.objectiveVM
              };
            });
          });
      },
      getCallOperatorSellerAppChart: function (userProfileId) {
        var url = '/api/profils/getCallOperatorSellerAppChart/{0}'.format(userProfileId);
        return http.get(url);
      }
    };

    return Service;
  });
