define({
  load: function (name, req, onload, config) {
    var info = localStorage.getItem('user-info');
    if (info) {
      info = JSON.parse(info);
    } else {
      info = {};
    }

    onload(info);
    window._usercontext = info;
  }
});
