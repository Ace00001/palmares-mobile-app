define([
  'app',
  'ioc!userService'
], function (app, userService) {
  'use strict';

  var dependencies = ['$state', '$ionicHistory', '$ionicLoading', '$timeout'];

  function ParametersController($state, $ionicHistory, $ionicLoading, $timeout) {
    var vm = this;

    vm.title = app.usercontext.fullName;
    vm.user = app.usercontext;

    vm.logout = function () {
      $ionicLoading.show({ template: 'Déconnexion en cours...' });
      userService.logoff();

      $timeout(function () {
        $ionicHistory.clearCache();
        $ionicHistory.clearHistory();
        $ionicLoading.hide();
        $state.go('login');
      }, 300);
    };

    vm.myBigStat = function () {
      $state.go('tab.userprofile-form', { id: app.usercontext.id });
    };

    vm.profil = function () {
      $state.go('tab.profile', { id: app.usercontext.id });
    };

    vm.showCompany = function () {
      return app.inRole('ceo');
    };

    vm.company = function () {
      $state.go('tab.company-form', { id: app.usercontext.companyId });
    };

    vm.privacy = function () {
      $state.go('tab.privacy');
    };

    vm.manage = function () {
      $state.go('tab.manage');
    };

    vm.showManage = function () {
      return app.inRole('director', 'ceo');
    }

    vm.parameters = function () {
      $state.go('tab.parameters');
    };
  }

  angular.module('app').controller('ParametersController', dependencies.concat(ParametersController));
});
