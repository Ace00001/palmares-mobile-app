var require = {
  baseUrl: 'app',
  paths: {
    'ionic': '../lib/ionic/js/ionic.bundle.min',
    'jquery': '../lib/jquery/jquery-1.12.0.min',
    'gmapapi': 'http://maps.googleapis.com/maps/api/js?libraries=places,geometry&sensor=false',
    'gmap': '../lib/angular-google-maps/dist/angular-google-maps',
    'slogger': '../lib/angular-simple-logger/dist/angular-simple-logger',
    'lodash': '../lib/lodash/dist/lodash',
    'moment': '../lib/moment/min/moment-with-locales.min',
    'ion-place-autocomplete': '../lib/ion-place-autocomplete/ion-place-autocomplete',
    'ioc': 'services/ioc',
    'chart': '../lib/chart.js/dist/Chart.min',
    'ngCordova': '../lib/ng-cordova/ng-cordova.min',
    'uiValidate': '../lib/angular-ui-validate/dist/validate.min',
    'angular-svg-round-progressbar': '../lib/angular-svg-round-progressbar/build/roundProgress.min',
  },
  shim: {
    'ionic': { deps: ['jquery'] },
    'slogger': { deps: ['ionic'] },
    'ion-place-autocomplete': { deps: ['gmapapi', 'ionic', 'gmap'] },
    'gmap': { deps: ['gmapapi', 'ionic', 'slogger', 'lodash'] },
    'ngCordova': { deps: ['ionic'] },
    'uiValidate': { deps: ['ionic'] },
    'angular-svg-round-progressbar': { deps: ['ionic'] }
  },
  priority: [
    'jquery',
    'ionic'
  ]
};
