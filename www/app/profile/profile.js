﻿define([
  'app',
  'ioc!profileService'
], function (app, profileService) {
  'use strict';

  var dependencies = ['$state', '$filter'];

  function ProfileController($state, $filter) {
    var vm = this;

    vm.profile = null;
    vm.infoBoxData = [];
    vm.goToPrize = function () {
      $state.go('tab.prize');
    };

    // TEST
    /*vm.getCallOperatorSellerAppChart = function () {
      var tmp = profileService.getCallOperatorSellerAppChart($state.params.id).then(function (r) {
        return r;
      });
      return tmp;
    };*/

  }

  angular.module('app').controller('ProfileController', dependencies.concat(ProfileController));
});
