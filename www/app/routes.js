define([
  'app'
], function (app) {
  'use strict';

  // definition of routes
  angular.module('app').config(['$stateProvider', '$urlRouterProvider', configRoute]);

  function configRoute($stateProvider, $urlRouterProvider) {
    // url routes/states
    $urlRouterProvider.otherwise('/splash');

    $stateProvider
      .state('splash', {
        url: '/splash',
        templateUrl: 'app/splash/splash.html',
        controller: 'SplashController',
        controllerAs: 'vm'
      })
      .state('login', {
        url: '/login',
        templateUrl: 'app/login/login.html',
        controller: 'LoginController',
        controllerAs: 'vm'
      })
      .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'app/tabs/tabs.html',
        controller: 'TabsController',
        controllerAs: 'vm'
      })
      .state('tab.prize', {
        url: '/prize',
        views: {
          'prize': {
            templateUrl: 'app/prize/prize.html',
            controller: 'PrizeController',
            controllerAs: 'vm'
          }
        }
      })
      .state('tab.profile', {
        url: '/profile',
        views: {
          'prize': {
            templateUrl: 'app/profile/profile.html',
            controller: 'ProfileController',
            controllerAs: 'vm'
          }
        }
      })
      .state('tab.parameters', {
        url: '/parameters',
        views: {
          'parameters': {
            templateUrl: 'app/parameters/parameters.html',
            controller: 'ParametersController',
            controllerAs: 'vm'
          }
        }
      })
      .state('tab.userprofile-form', {
        cache: false,
        url: '/userprofile-form/:id',
        views: {
          'parameters': {
            templateUrl: 'app/userprofile/userprofile-form.html',
            controller: 'UserProfileFormController',
            controllerAs: 'vm'
          }
        }
      })
      .state('tab.change-password', {
        cache: false,
        url: '/change-password/:id',
        views: {
          'parameters': {
            templateUrl: 'app/userprofile/change-password.html',
            controller: 'ChangePasswordController',
            controllerAs: 'vm'
          }
        }
      })
      .state('tab.branch-form', {
        cache: false,
        url: '/branch-form/:id',
        views: {
          'parameters': {
            templateUrl: 'app/branch/branch-form.html',
            controller: 'BranchFormController',
            controllerAs: 'vm'
          }
        }
      })
      .state('tab.company-form', {
        cache: false,
        url: '/company-form/:id',
        views: {
          'parameters': {
            templateUrl: 'app/company/company-form.html',
            controller: 'CompanyFormController',
            controllerAs: 'vm'
          }
        }
      })
      .state('tab.manage', {
        cache: false,
        url: '/manage',
        views: {
          'parameters': {
            templateUrl: 'app/manage/manage.html',
            controller: 'ManageController',
            controllerAs: 'vm'
          }
        }
      })
      .state('tab.search-list-filter', {
        url: '/filter/:view/:param1',
        views: {
          'search': {
            templateUrl: 'app/listfilter/list-filter.html',
            controller: 'ListFilterController',
            controllerAs: 'vm'
          }
        }
      })
      .state('tab.prize-list-filter', {
        url: '/filter/:view/:param1',
        views: {
          'prize': {
            templateUrl: 'app/listfilter/list-filter.html',
            controller: 'ListFilterController',
            controllerAs: 'vm'
          }
        }
      })
      ;
  }

});
