define([
  'app',
  'ioc!companyService',
], function (app, companyService) {
  'use strict';

  var dependencies = ['$state', '$scope', '$stateParams', '$ionicPopover', '$ionicLoading'];
  angular.module('app')
    .controller('CompanyFormController', dependencies.concat(CompanyFormController));

  function CompanyFormController($state, $scope, $stateParams, $ionicPopover, $ionicLoading) {
    var vm = this;

    vm.viewTitle = 'Société';
    vm.companyId = null;

    vm.company = {
      name: '',
      phone: '',
      email: '',
      address: {
        text: null,
        latitude: 48.852063,
        longitude: 2.355841,
        street_number: '',
        route: '',
        postal_code: '',
        locality: '',
        country: ''
      },
      companyId: '00000000-0000-0000-0000-000000000000',
      photoId: '',
    };


    vm.isEditView = function () {
      return vm.companyId != '';
    }

    vm.submit = function () {
      if (vm.isEditView()) {
        vm.company.companyId = vm.companyId;
        companyService.putCompany(vm.company)
          .done(function (r) {
            $state.go('tab.parameters');
          });
      }
    };

    // ADDRESS
    // https://developers.google.com/maps/documentation/javascript/examples/places-autocomplete-addressform
    var componentForm = {
      street_number: 'short_name',
      route: 'long_name',
      postal_code: 'short_name',
      locality: 'long_name',
      country: 'long_name'
    };

    vm.locationchanged = function (it) {
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({ 'address': it }, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
          var place = results[0];
          vm.company.address.longitude = place.geometry.location.lng();
          vm.company.address.latitude = place.geometry.location.lat();

          // Get each component of the address from the place details
          // and fill the corresponding field on the form.
          for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
              var val = place.address_components[i][componentForm[addressType]];
              vm.company.address[addressType] = val;
            }
          }
        } else {
          console.trace('Geocode was not successful for the following reason: ' + status);
        }
      });
    };

    function loadData() {
      vm.companyId = $stateParams.id;
      if (vm.isEditView()) {
        vm.viewTitle = "Modifier une société";
        companyService.getCompany(vm.companyId)
          .done(function (r) {
            vm.company = r;
          });
      }
    }

    vm.doRefresh = function () {
      loadData();
      $scope.$broadcast('scroll.refreshComplete');
    };

    loadData();

    //PHOTO ---------------------------------------------
    vm.imageData = null;
    vm.errors = []; // errors from pictureService

    var template = $('#popover-picture').html();
    vm.pop = $ionicPopover.fromTemplate(template, {
      scope: $scope
    });

    vm.showPicturePopover = function (e) {
      vm.pop.show(e);
    };

    vm.takePhoto = function () {
      vm.errors = [];
      pictureService.takePhoto()
        .then(function (imageData) {
          vm.imageData = imageData;
          vm.pop.hide();
        },
        function (error) {
          vm.errors.push(error);
        });
    };

    vm.selectPhoto = function () {
      vm.errors = [];
      pictureService.selectPhoto()
        .then(function (imageData) {
          vm.imageData = imageData;
          vm.pop.hide();
        },
        function (error) {
          vm.errors.push(error);
        });
    };

    vm.cancelPhoto = function () {
      vm.imageData = null;
      vm.errors = [];
      $ionicScrollDelegate.scrollTop();
    };

    vm.sendPhoto = function () {
      $ionicLoading.show({
        template: 'Chargement ...'
      });
      vm.errors = [];
      var args = {
        encoded: vm.imageData
      };
      companyService.sendPhoto(vm.companyId, args)
        .success(function (data) {
          vm.imageData = null;
          vm.status = 'Sent';
          loadData();
          $ionicLoading.hide();
        })
        .error(function (error) {
          if (error.message) {
            vm.errors.push(error.message);
            $ionicLoading.hide();
            return;
          }
          vm.errors.push('Une erreur s\'est produite.');
          $ionicLoading.hide();
        });
    };

  }

});

