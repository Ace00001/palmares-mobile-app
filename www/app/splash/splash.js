define([
  'app',
  'ioc!userService'
], function (app, userService) {
  'use strict';

  var dependencies = ['$state', '$ionicHistory', '$timeout'];

  function SplashController($state, $ionicHistory, $timeout) {
    var vm = this;

    $timeout(function () {
      userService.authorized()
        .done(function (r) {
          if (r) {
            $state.go('tab.prize');
          } else {
            $state.go('login');
          }
        });
    }, 1000);
  }

  angular.module('app').controller('SplashController', dependencies.concat(SplashController));

});
