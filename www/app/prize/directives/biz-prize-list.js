define(['app', 'ioc!profileService'], function (app, profileService, $ionicSlideBoxDelegate) {
  angular.module('app').directive('bizPrizeList', bizPrizeList);

  function bizPrizeList() {
    return {
      restrict: 'AE',
      templateUrl: 'app/prize/directives/biz-prize-list.html',
      scope: {
        bizRole: '@',
        bizForPicker: '@',
        bizItems: '=?'
      },
      controller: ['$scope', '$state','$ionicSlideBoxDelegate', bizPrizeListController],
      controllerAs: 'vm'
    };
  }

  function bizPrizeListController($scope, $state, $ionicSlideBoxDelegate) {
    $scope.bizItems = [];
    var vm = this;
    // vm.selectedMonth = 'March';
    vm.months = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'];
    vm.salesRole = 'caDone';
    $scope.slideIndex = 0;
    vm.directorSlides = [{name: "revenue",sort: 'caDone'}, {name: "réussite sur RDV prospect",sort: 'successRateBySellerAppProspect'},{name:"réussite sur RDV client",sort : 'successRateBySellerAppClient'},{name: "vente annulée",sort:'saleCanceledRate'},{name: "panier moyen",sort:'averageBasket'}];
    vm.callSlides = [{name: "RDV exploité",sort:'sellerAppDone'}, {name: "Taux de fiabilité",sort:'reliabilityRate'},{name:"Taux de vente sur RDV exploité",sort:'successRateSaleOnRdvUncanceled'},{name: "Taux de vente sur RDV total",sort:'successRateSaleOnRdvTotal'}];
    vm.technicianSlides = [{name: "revenue", sort: 'caAchieved'}, {name: "quantité", sort: 'quantity'},{name:"panier moyen", sort: 'averageBasket'}];
    vm.items = $scope.bizItems;
    vm.role = $scope.bizRole;
    vm.forPicker = $scope.bizForPicker;
    vm.multiPicker = $state.params.multi;
    vm.progressStyle = "{'background': 'linear-gradient(180deg, white 20%, rgba(255,0,0,0.5), rgba(255,0,0,1));'}";
    vm.filterActive = false;
    vm.isFirstSegment = true;
    vm.activeRole = null;

    var time = moment().format("YYYY-MM-DD");

    var timeArr = time.split('-');
    var timeMonth = timeArr[1];
    var date = new Date();
    $scope.getMonthName = vm.months[date.getMonth()];
    console.log($scope.getMonthName);
    if(vm.role == 'director'){
        vm.activeRole = 'seller';
        vm.salesRole = vm.directorSlides[0].sort;
    }
    if(vm.role == 'calloperator'){
        vm.activeRole = 'calloperator';
        vm.salesRole = vm.callSlides[0].sort;
    }
    if(vm.role == 'technician'){
        vm.activeRole = 'technician';
        vm.salesRole = vm.technicianSlides[0].sort;
    }
    vm.getRoleFilters = function(){
        if(vm.role == 'director'){
            return vm.directorSlides;
        }
        if(vm.role == 'calloperator'){
            return vm.callSlides;
        }
        if(vm.role == 'technician'){
            return vm.technicianSlides;
        }
    };
    vm.goToProfile = function (profile) {
      if (this.forPicker && !this.multiPicker) {
        $scope.$parent.vm.onSelect(profile);
        return;
      }

      if (this.forPicker && this.multiPicker) {
        return;
      }

      $state.go('tab.profile', { id: profile.id, role: profile.role });
    };

    vm.setSegmentMode = function (value) {
        if(vm.role == 'director'){
              vm.isFirstSegment = value;
          
            if(value){
              //salesman
              if(vm.activeRole != 'seller'){
                vm.activeRole = 'seller';
                loadNewData();
                console.log('load seller');  
              }
            }else{
              //salesman manager
              if(vm.activeRole != 'director'){
                vm.activeRole =  'director';
                loadNewData();
                console.log('load manager');
              }    
            }
          
        }
        
      else if(vm.role == 'calloperator'){
        vm.isFirstSegment = value;
          
            if(value){
              //calloperator
              if(vm.activeRole != 'calloperator'){
                vm.activeRole = 'calloperator';
                loadNewData();
                console.log('load calloperator');  
              }
            }
            // else{
            //   //calloperator manager
            //   if(vm.activeRole != 'director'){
            //     vm.activeRole =  'director';
            //     loadNewData();
            //     console.log('load manager');
            //   }    
            // }

      }
      else if(vm.role == 'technician'){
        vm.isFirstSegment = value;
          
            if(value){
              //technician
              if(vm.activeRole != 'technician'){
                vm.activeRole = 'technician';
                loadNewData();
                console.log('load technician');  
              }
            }
            // else{
            //   //technician manager
            //   if(vm.activeRole != 'director'){
            //     vm.activeRole =  'director';
            //     loadNewData();
            //     console.log('load manager');
            //   }    
            // }
      }
      
    }

    vm.onFilter = function (filter) {
      console.log(filter)
      vm.filterActive = true;
      // fetchdata(filter);
      loadNewData(filter);
    };
    vm.next = function(index, items) {
      console.log('lenght:', vm.getRoleFilters($ionicSlideBoxDelegate.slidesCount()).length);
      console.log('index : ', index)
      console.log('index : ', index++)
      if(index < vm.getRoleFilters($ionicSlideBoxDelegate.slidesCount()).length){
        if($ionicSlideBoxDelegate.currentIndex()+1 < vm.getRoleFilters($ionicSlideBoxDelegate.slidesCount()).length){
          $scope.getIndex = $ionicSlideBoxDelegate.currentIndex();
        //   console.log($scope.getIndex+1);
        //   console.log(vm.getRoleFilters()[$scope.getIndex+1].name);
        //   console.log(vm.getRoleFilters()[$scope.getIndex+1].sort);
          }
          if (vm.role == 'director'){
            $ionicSlideBoxDelegate.$getByHandle('sliderone').next();
          }
          else if (vm.role == 'calloperator'){
            $ionicSlideBoxDelegate.$getByHandle('slidertwo').next();
          }
          else{
            $ionicSlideBoxDelegate.$getByHandle('sliderthree').next();
          }
          loadNewData(vm.activeRole , $scope.myFilter);  
      }
    };
    
    vm.previous = function(index, items) {
      if(index > 0){
        // console.log('lenght:', vm.getRoleFilters($ionicSlideBoxDelegate.slidesCount()).length);
        if($ionicSlideBoxDelegate.currentIndex() > 0){
            $scope.getIndex = $ionicSlideBoxDelegate.currentIndex();
        //     console.log($ionicSlideBoxDelegate.currentIndex()-1);
        //     console.log(vm.getRoleFilters()[$scope.getIndex-1].name);
        //     console.log(vm.getRoleFilters()[$scope.getIndex-1].sort);  
        }
        
        // // if($ionicSlideBoxDelegate.currentIndex()$ionicSlideBoxDelegate.slidesCount()){
        //     $scope.getIndex = $ionicSlideBoxDelegate.currentIndex();
        //     console.log($scope.getIndex);
        //     if(($scope.getIndex-1)>$ionicSlideBoxDelegate.slidesCount()){
        //       console.log(vm.directorSlides[$scope.getIndex-1].name)
        //       console.log(vm.directorSlides[$scope.getIndex-1].sort)
        //     }
            
        //     $ionicSlideBoxDelegate.next();
        //     getFilter();
        // // }
          // $ionicSlideBoxDelegate.previous();
          if (vm.role == 'director'){
            $ionicSlideBoxDelegate.$getByHandle('sliderone').previous();
          }
          else if (vm.role == 'calloperator'){
            $ionicSlideBoxDelegate.$getByHandle('slidertwo').previous();
          }
          else{
            $ionicSlideBoxDelegate.$getByHandle('sliderthree').previous();
          }

          loadNewData(vm.activeRole , $scope.myFilter);
      }
      // getFilter();
   };
   vm.showSelectValue = function(selected){
      console.log(selected);
      $scope.getMonthName = selected;
      loadNewData(vm.activeRole , $scope.myFilter);
   };

  function getFilter(){
    if($scope.roleSort == undefined || $scope.roleSort == null) {
      console.log('IN ROLESORT');
      console.log(vm.activeRole);
      if(vm.activeRole == 'seller'){
        $scope.roleSort = vm.directorSlides[0].sort;
      }
      if(vm.activeRole == 'technician'){
       
        $scope.roleSort = vm.technicianSlides[0].sort;
      }
      if(vm.activeRole == 'calloperator'){
        $scope.roleSort = vm.callSlides[0].sort;
      }
    }
    var params = {
        roleName: $scope.roleName,
        month: $scope.getMonthName,
        sort: $scope.roleSort
    }
    console.log(params);
    $scope.filter = {
        criterias: {
           single: 'month',
           sort: params.sort,
           month : params.month
        }
    }
    if($scope.roleName){
      vm.salesRole = $scope.roleSort;
    }
  };
  // Called each time the slide changes
  $scope.slideChanged1 = function(index) {
    $scope.slideIndex = index;
    console.log('CurrentIndex1:!',index);
    $scope.roleName = vm.getRoleFilters()[index].name;
    $scope.roleSort = vm.getRoleFilters()[index].sort;
    // getFilter();
    // console.log(vm.getRoleFilters()[index].name)
    // console.log(vm.getRoleFilters()[index].sort)

  };
  $scope.slideChanged2 = function(index) {
    $scope.slideIndex = index;
    console.log('CurrentIndex2:!',index);
    $scope.roleName = vm.getRoleFilters()[index].name;
    $scope.roleSort = vm.getRoleFilters()[index].sort;
    // getFilter();
  };
  $scope.slideChanged3 = function(index) {
    $scope.slideIndex = index;
    console.log('CurrentIndex3:!',index);
    $scope.roleName = vm.getRoleFilters()[index].name;
    $scope.roleSort = vm.getRoleFilters()[index].sort;
    // getFilter();
  };
    function fetchdata(filter) {
      profileService.getList(vm.role, filter)
        .done(function (r) {
          vm.items.length = 0;
          vm.items.addRange(r);
          safeApply($scope);
          console.log(vm.items);
        });
    };

    function loadNewData(role , filter) {
      // var filterd = //{}
      // {
      //     criterias: {
      //        single: 'january',
      //        sort: 'successRateBySellerAppProspect'
      //     }
      // }
      // console.log(JSON.stringify(filterd))
      // profileService.getList(vm.activeRole, filterd)
      getFilter();
      var myFilter = $scope.filter;
      console.log(myFilter);
      profileService.getList(vm.activeRole, myFilter)
        .done(function (r) {
          vm.items.length = 0;
          vm.items.addRange(r);
          safeApply($scope);
          console.log(vm.items);
        });
    };

    function clearFilterInput() {
      if (vm.filterActive) {
        vm.filterActive = false;
        $scope.$broadcast('RESET_FILTER');
      }
    };

    vm.doRefresh = function () {
      fetchdata();
      clearFilterInput();
      $scope.$broadcast('scroll.refreshComplete');
    };
    // getFilter();
    // fetchdata();
    loadNewData();
  };
 
});
