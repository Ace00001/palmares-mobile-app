define([
  'app',
  'ioc!profileService',
  'ioc!userService'
], function (app, profileService, userService) {
  'use strict';

  var dependencies = [];

  function PrizeController($scope) {
    var vm = this;

    // vm.roleSet = ["director", "seller", "calloperator", "technician", "agence"];
    vm.roleSet = ["director", "calloperator", "technician"];

    // retire le role de l'utilisateur courant, puis le rajoute au début du tableau.
    // afin qu'il soit le premier afficher.
    var role = app.usercontext.role;
    if(role == 'ceo'){
      role = 'director'
    }
    vm.roleSet.splice(vm.roleSet.indexOf(role), 1);
    vm.roleSet.unshift(role);
    function fetchdata() {
      userService.getUserSet().done(function (response) {
        console.log(response);
      }).fail(function(error){
        console.log(error);
      });
    }

    // fetchdata();
    vm.nextSlide = function (rol){
      if(rol == 'director'){
        vm.swiper.slideTo(1);  
      }else {
        vm.swiper.slideTo(2);
      }
    };
    // setTimeout(function(){nextSlide()},4000);
    vm.prev = function (rol){
      if(rol == 'technician'){
        vm.swiper.slideTo(1);  
      }else {
        vm.swiper.slideTo(0);
      }
      
    };

    vm.swiperOptions = {
        /* Whatever options */
        effect: 'slide',
        initialSlide: 0,
        /* Initialize a scope variable with the swiper */
        onInit: function(swiper){
        vm.swiper = swiper;
        },
        onSlideChangeEnd: function(swiper){
        console.log('The active index is ' + swiper.activeIndex); 
        }
      };

    function getData() {
      userService.getByRole(role).done(function (response) {
        console.log(response);
      }).fail(function(error){
        console.log(error);
      });
    }

    getData();
  }

  angular.module('app').controller('PrizeController', dependencies.concat(PrizeController));
});
