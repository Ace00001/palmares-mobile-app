define([
  'app'
], function (app) {
  'use strict';

  var dependencies = ['$scope'];

  function SmallMapController($scope) {
    var vm = this;

    var location = $scope.$parent.getLocation ?
      $scope.$parent.getLocation()
      : {
        latitude: 48.852063,
        longitude: 2.355841
      };

    vm.map = { center: location, zoom: 8 };
  }

  angular.module('app').controller('SmallMapController', dependencies.concat(SmallMapController));
});
