define([
  'app',
  'ioc!userService'
], function (app, userService) {
  'use strict';

  var dependencies = ['$ionicHistory', '$ionicNavBarDelegate', '$state'];

  function LoginController($ionicHistory, $ionicNavBarDelegate, $state) {
    var vm = this;

    $ionicHistory.clearHistory()

    vm.login = function () {
      userService.login(this.email, this.password)
        .done(function (r) {
          if (r) {
            $state.go('tab.prize');
          }
        }).fail(function (r) {
          if (r) {
            vm.loginFail = true;
          }
        });
    };
    vm.email = null;
    vm.password = null;
    vm.loginFail = false;
  }

  angular.module('app').controller('LoginController', dependencies.concat(LoginController));

});
