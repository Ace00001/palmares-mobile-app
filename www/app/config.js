define([
  'app'
], function (app) {
  'use strict';

  // additional config-blocks
  angular.module('app').run(["$ionicLoading", "$state", ApiURL]);

  function ApiURL($ionicLoading, $state) {
    app.ionicLoading = $ionicLoading;
    app.$state = $state;
    //app.API_URL = 'http://develop.bigstat.ovh';
    app.API_URL = 'http://mybigstat.com:8585';
  }

  angular.module('app').config(function ($ionicConfigProvider) {
    $ionicConfigProvider.tabs.position('bottom')
  });

});
