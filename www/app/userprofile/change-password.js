define([
  'app',
  'ioc!userService',
], function (app, userService) {
  'use strict';

  var dependencies = ['$state', '$scope', '$stateParams'];
  angular.module('app').controller('ChangePasswordController', dependencies.concat(ChangePasswordController));

  function ChangePasswordController($state, $scope, $stateParams) {
    var vm = this;

    vm.viewTitle = 'Modifier le mot de passe';
    vm.userProfileId = null;

    vm.userProfile = {
      firstName: '',
      lastName: '',
      phone: '',
      email: '',
      password: '',
      confirm_password: '',
      role: 'Seller',
      branchId: '00000000-0000-0000-0000-000000000000',
      branchName: 'Unknown Branch',
      photoId: '',
      archived: false
    };

    vm.changePasswordBindingModel = {
      oldPassword: '',
      newPassword: '',
      confirmPassword: ''
    }

    vm.submit = function () {
      userService.changePassword(vm.changePasswordBindingModel)
        .done(function (r) {
          $state.go('tab.manage');
        });
    };

    vm.userProfileId = $stateParams.id;

    userService.getUser(vm.userProfileId)
      .done(function (r) {
        vm.userProfile = r;
      });
  }

});

