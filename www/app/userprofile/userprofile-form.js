define([
  'app',
  'ioc!userService',
  'ioc!branchService'
], function (app, userService, branchService) {
  'use strict';

  var dependencies = ['$state', '$scope', '$stateParams', '$ionicScrollDelegate',
    '$ionicPopover', '$ionicLoading', 'pictureService'
  ];
  angular.module('app')
    .controller('UserProfileFormController', dependencies.concat(UserProfileFormController));

  function UserProfileFormController($state, $scope, $stateParams, $ionicScrollDelegate,
    $ionicPopover, $ionicLoading, pictureService) {
    var vm = this;

    vm.viewTitle = 'Utilisateur';
    vm.userProfileId = '';

    vm.userProfile = {
      firstName: '',
      lastName: '',
      phone: '',
      email: '',
      password: '',
      confirm_password: '',
      role: 'Seller',
      branchId: '00000000-0000-0000-0000-000000000000',
      branchName: 'Unknown Branch',
      photoId: '',
      archived: false
    };

    vm.roleSet = [
      { text: "Commercial", value: "Seller" },
      { text: "Prospecteur", value: "CallOperator" },
      { text: "Technician", value: "Agent de service" }
    ];;

    vm.branchSet = []; // branch set for CEO

    vm.isEditView = function () {
      return vm.userProfileId != '';
    }

    vm.isCreateView = function () {
      return !vm.isEditView();
    }

    vm.changePassword = function () {
      $state.go('tab.change-password', { id: vm.userProfileId });
    };

    vm.showBranch = function () {
      var isCEO = app.inRole('ceo');
      var isCreateView = vm.isCreateView();
      var isManyBranches = vm.branchSet.length > 1;
      var result = (isCEO && isCreateView && isManyBranches);
      return result;
    }

    function isCEO() {
      return app.inRole('ceo');
    }

    vm.isEditedUserSameAsCurrentUser = function () {
      return vm.userProfileId == app.usercontext.id;
    }

    vm.submit = function () {
      if (vm.isEditView()) {
        userService.editUser(vm.userProfile)
          .done(function (r) {
            userService.authorized();
            $state.go('tab.manage');
          });
      } else {
        userService.addUser(vm.userProfile)
          .done(function (r) {
            $state.go('tab.manage');
          });
      }
    };

    vm.putBigZ = function () {
      userService.putBigZ(vm.userProfile.bigZ)
        .done(function (r) {
          // TODO : que faire une fois le BigZ modifié ?
        });
    };

    vm.showDaysOff = function () {
      var result = (
        app.inRole('director', 'ceo')
        && vm.isEditView()
      );

      return result;
    }

    vm.showArchive = function () {
      // current user is director or CEO
      // is modify view
      // the modified user is not archived
      // edited user is different than current user (can't archive himself)

      var result = (
        app.inRole('director', 'ceo')
        && vm.isEditView()
        && !vm.isEditedUserSameAsCurrentUser()
      );

      return result;
    }

    vm.archive = function () {
      userService.archive(vm.userProfileId)
        .success(function () {
          vm.userProfile.archived = true;
          $ionicScrollDelegate.scrollTop();
        });
    }

    vm.unarchive = function () {
      userService.unarchive(vm.userProfileId)
        .success(function () {
          vm.userProfile.archived = false;
          $ionicScrollDelegate.scrollTop();
        });
    }



    function loadData() {
      vm.userProfileId = $stateParams.id;
      if (vm.isEditView()) {
        if (vm.isEditedUserSameAsCurrentUser()) {
          vm.viewTitle = "Mon compte";
        } else {
          vm.viewTitle = "Modifier un utilisateur";
        }
        userService.getUser(vm.userProfileId)
          .done(function (r) {
            vm.userProfile = r;
          });
      } else {
        vm.viewTitle = "Ajouter un utilisateur";
      }

      if (isCEO() && vm.isCreateView()) {
        branchService.getBranchSet().done(function (r) {
          vm.branchSet = r;
          vm.userProfile.branchId = vm.branchSet[0].id;
        });
      }
    }

    vm.doRefresh = function () {
      loadData();
      $scope.$broadcast('scroll.refreshComplete');
    };

    loadData();

    //PHOTO ---------------------------------------------
    vm.imageData = null;
    vm.errors = []; // errors from pictureService

    var template = $('#popover-picture').html();
    vm.pop = $ionicPopover.fromTemplate(template, {
      scope: $scope
    });

    vm.showPicturePopover = function (e) {
      vm.pop.show(e);
    };

    vm.takePhoto = function () {
      vm.errors = [];
      pictureService.takePhoto()
        .then(function (imageData) {
          vm.imageData = imageData;
          vm.pop.hide();
        },
        function (error) {
          vm.errors.push(error);
        });
    };

    vm.selectPhoto = function () {
      vm.errors = [];
      pictureService.selectPhoto()
        .then(function (imageData) {
          vm.imageData = imageData;
          vm.pop.hide();
        },
        function (error) {
          vm.errors.push(error);
        });
    };

    vm.cancelPhoto = function () {
      vm.imageData = null;
      vm.errors = [];
      $ionicScrollDelegate.scrollTop();
    };

    vm.sendPhoto = function () {
      $ionicLoading.show({
        template: 'Chargement ...'
      });
      vm.errors = [];
      var args = {
        encoded: vm.imageData
      };
      userService.sendPhoto(vm.userProfileId, args)
        .success(function (data) {
          vm.imageData = null;
          vm.status = 'Sent';
          loadData();
          $ionicLoading.hide();
        })
        .error(function (error) {
          if (error.message) {
            vm.errors.push(error.message);
            $ionicLoading.hide();
            return;
          }
          vm.errors.push('Une erreur s\'est produite.');
          $ionicLoading.hide();
        });
    };

  }

});

