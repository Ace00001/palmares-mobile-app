define(['app', 'ioc!userService'], function (app, userService) {
  angular.module('app').directive('bizUserDaysOff', bizUserDaysOff);

  function bizUserDaysOff() {
    return {
      restrict: 'AE',
      templateUrl: 'app/userprofile/directives/biz-user-days-off.html',
      scope: {
        bizUserProfile: '='
      },
      controller: ['$scope', '$state', '$ionicScrollDelegate', bizUserDaysOffController],
      controllerAs: 'vm'
    };
  }

  function bizUserDaysOffController($scope, $state, $ionicScrollDelegate) {
    var vm = this;

    vm.userProfile = $scope.bizUserProfile;
    vm.dayOffSet = [];

    vm.newDayOffInterval = {
      startDate: new Date(),
      endDate: new Date()
    }

    $scope.$watch('bizUserProfile', function (newValue, oldValue) {
      vm.userProfile = newValue;
      fetchdata();
    });

    vm.isDivShown = false;
    vm.toggleDiv = function () {
      vm.isDivShown = !vm.isDivShown;
      $ionicScrollDelegate.scrollBottom();
    };

    vm.addDayOffInterval = function () {
      var userProfileId = vm.userProfile.userProfileId;
      userService.addDayOffInterval(userProfileId, vm.newDayOffInterval)
        .done(function (r) {
          fetchdata();
        });
    };

    vm.removeDayOffInterval = function (dayOffInterval) {
      userService.removeDayOffInterval(vm.userProfile.userProfileId, dayOffInterval)
        .done(function (r) {
          fetchdata();
        });
    };

    function fetchdata() {
      if (angular.isDefined(vm.userProfile.userProfileId)) {
        userService.getDayOffSet(vm.userProfile.userProfileId).done(function (r) {
          vm.dayOffSet.length = 0;
          vm.dayOffSet.addRange(r);
          safeApply($scope);
        });
      }
    }

    fetchdata();
  }

});
