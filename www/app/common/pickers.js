define(["app", "./Messenger"], function (app, messenger) {
  return {
    pickpands: function () {
      app.$state.go('tab.pands-picker');
      return messenger.register('pick-pands', true);
    },
    pickClient: function () {
      app.$state.go('tab.client-picker');
      return messenger.register('pick-client', true);
    },
    createClient: function () {
      app.$state.go('tab.client-form');
      return messenger.register('create-client', true);
    },
    editClient: function (client) {
      app.$state.go('tab.client-form', { clientId: client.id });
      return messenger.register('edit-client', true);
    },
    pickProfile: function (role, multi) {
      app.$state.go('tab.profile-picker', { role: role, multi: multi });
      return messenger.register('pick-profile', true);
    }
  };
});
