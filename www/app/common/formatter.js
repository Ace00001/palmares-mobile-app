define({
  ordinal: function (val) {
    if (val == 1) {
      return val + "er";
    } else {
      return val + "e";
    }
  },
  percent: function (val) {
    return Math.round(val) + '%';
  },
  round: function (val) {
    if (val < 1000) {
      return val;
    } else {
      return Math.round(val / 1000);
    }
  },
  money: function (val, symbol) {
    if (val < 1000) {
      return Math.round(val) + " " + (symbol ? symbol : '');
    } else {
      return Math.round(val / 1000) + " k" + (symbol ? symbol : '');
    }
  },
  double: function (val, decimal) {
    if (!decimal) {
      decimal = 0;
    } else {
      return Number(val).toFixed(decimal);
    }
  },
  saledisplay: function (val) {
    if (val <= 1) {
      return val + " Vente";
    } else {
      return val + " Ventes";
    }
  }
  ,
  servicedisplay: function (val) {
    if (val <= 1) {
      return val + " Service";
    } else {
      return val + " Services";
    }
  }
  ,
  annuledisplay: function (val) {
    if (val <= 1) {
      return val + " Annulée";
    } else {
      return val + " Annulées";
    }
  }
});
