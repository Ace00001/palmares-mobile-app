define([], function () {
  return {
    appointmentStatus: function (r) {
      // "confirmed", "sale", "negotiate","canceled"
      if (r == 'V') {
        return 'confirmed';
      }
      if (r == 'SS') {
        return 'sale';
      }
      if (r == 'X') {
        return 'canceled';
      }
      if (r == 'NEGO') {
        return 'negotiate';
      }
      return r;
    }
  };
})
