define({
  filterTime: function (list, dateField, criteria) {
    var today = moment();
    if (criteria == "today") {
      today = today.format("YYYY MM DD");
      return list.where(function (it) {
        var d = it[dateField];
        var check = moment(d).format("YYYY MM DD");
        return today == check;
      });
    } else if (criteria == "thisweek") {
      var todayWeek = moment(today).week();
      var todayYear = moment(today).year();
      return list.where(function (it) {
        var d = it[dateField];
        var check = moment(d).week();
        var checkYear = moment(d).year();
        return todayWeek == check && todayYear == checkYear;
      });
    } else if (criteria == "thismonth") {
      today = today.format("YYYY MM");
      return list.where(function (it) {
        var d = it[dateField];
        if (angular.isDefined(d)) {
          var check = moment(d).format("YYYY MM");
          return today == check;
        } else {
          return false;
        }
      });
    } else {
      return list;
    }
  },
  filterMultiMatch: function (list, field, criterias) {
    return list.where(function (it) {
      var v = it[field];
      if (!criterias || criterias.length == 0) {
        return true;
      } else if (criterias.indexOf(v) >= 0) {
        return true;
      } else {
        return false;
      }
    })
  }
});
