define([
  'app',
  'common/formatter'
], function (app, formatter) {
  'use strict';
  angular.module('app').filter('statboxvaluedisplay', [statboxvaluedisplay]);

  function statboxvaluedisplay() {
    var Filter = function (val, arg) {
      if (!val) {
        return val;
      }
      if (arg == 'appointment') {
        if (app.inRole('seller')) {
          return val.value;
        }
        return "{0}/{1}".format(val.value, val.total);
      }
      if (arg == 'holding') {
        if (app.inRole('calloperator')) {
          return "{0}/{1}".format(val.value, val.total);
        }
        return "{0}/{1}".format(formatter.round(val.value), formatter.money(val.total, '€'));
      }
      if (arg == 'pands') {
        return "{0}/{1}".format(val.value, val.total);
      }
      if (arg == 'facture' || arg == 'ca') {
        return formatter.money(val.value, '€');
      }
      if (arg == 'stock') {
        return val.value;
      }
      return val;
    }
    return Filter;
  }
});
