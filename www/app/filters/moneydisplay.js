define([
  'app',
  'common/formatter'
], function (app, formatter) {
  'use strict';
  angular.module('app').filter('moneydisplay', [moneydisplay]);

  function moneydisplay() {
    var Filter = function (val, arg) {
      return formatter.money(val, arg);
    }
    return Filter;
  }
});
