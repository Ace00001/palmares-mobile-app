define([
  'app'
], function (app) {
  'use strict';
  angular.module('app').filter('dayoffintervaldisplay', [dayoffintervaldisplay]);

  function dayoffintervaldisplay() {
    var Filter = function (val) {
      if (!val) {
        return val;
      }
      if (val.startDate.isSame(val.endDate, 'day')) {
        return "Le " + val.startDate.format("D MMMM YYYY");
      } else if (val.startDate.isSame(val.endDate, 'month')) {
        return "Du " + val.startDate.format("D") + " au " + val.endDate.format("D MMMM YYYY");
      } else if (val.startDate.isSame(val.endDate, 'year')) {
        return "Du " + val.startDate.format("D MMMM") + " au " + val.endDate.format("D MMMM YYYY");
      } else {
        return "Du " + val.startDate.format("D MMMM YYYY") + " au " + val.endDate.format("D MMMM YYYY");
      }
    }
    return Filter;
  }
});
