define([
  'app'
], function (app) {
  'use strict';

  angular.module('app').filter('roletext', [roletext]);

  function roletext() {
    var Filter = function (val, arg) {
      val = val.toUpperCase();
      var role = "";
      switch (val) {
        case 'calloperator'.toUpperCase():
          role = 'Prospecteur';
          break;
        case 'seller'.toUpperCase():
          role = 'Commercial';
          break;
        case 'director'.toUpperCase():
          role = 'Vendeur';
          break;
        case 'agence'.toUpperCase():
          role = 'Agence';
          break;
        case 'technician'.toUpperCase():
          role = 'Agent de service';
          break;
        case 'directorAndSeller'.toUpperCase():
          role = 'Directeur / Commercial';
          break;
          case 'directorAndSellerAndCalloperator'.toUpperCase():
          role = 'Directeur / Commercial / Prospecteur';
          break;
        default:
          role = val;
          break;
      }

      return role;
    }
    return Filter;
  }
});
