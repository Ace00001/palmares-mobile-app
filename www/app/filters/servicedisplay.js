define([
  'app',
  'common/formatter'
], function (app, formatter) {
  'use strict';
  angular.module('app').filter('servicedisplay', [servicedisplay]);

  function servicedisplay() {
    var Filter = function (val) {
      return formatter.servicedisplay(val);
    }
    return Filter;
  }
});
