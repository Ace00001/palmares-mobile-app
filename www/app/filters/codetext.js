define([
  'app'
], function (app) {
  'use strict';

  angular.module('app').filter('codetext', [codetext]);

  function codetext() {
    var Filter = function (val, arg) {
      var text = "";

      if (angular.isDefined(val)) {
        val = val.toUpperCase();
        switch (val) {
          // TYPE CLIENT
          case 'prospect'.toUpperCase():
            text = 'Prospect';
            break;
          case 'client'.toUpperCase():
            text = 'Client';
            break;
          case 'retracted'.toUpperCase():
            text = 'Rétracté';
            break;
          // DATE - FILTRE DATE
          case 'week'.toUpperCase():
            text = 'Semaine';
            break;
          case 'month'.toUpperCase():
            text = 'Mois';
            break;
          case 'year'.toUpperCase():
            text = 'Année';
            break;
          case 'all'.toUpperCase():
            text = 'Tout';
            break;
          case 'today'.toUpperCase():
            text = 'Aujourd’hui';
            break;
          case 'thisweek'.toUpperCase():
            text = 'Cette semaine';
            break;
          case 'thismonth'.toUpperCase():
            text = 'Ce mois ci';
            break;
          case 'nofilter'.toUpperCase():
            text = 'Indifférent';
            break;
          // CA
          case 'caDone'.toUpperCase():
            text = 'CA Tenu';
            break;
          case 'caCharged'.toUpperCase():
            text = 'CA facturé';
            break;
          case 'caAchieved'.toUpperCase():
            text = 'CA accompli';
            break;
          // RDV
          case 'sellerAppDone'.toUpperCase():
            text = 'RDV tenu';
            break;
          // RATE
          case 'reliabilityRate'.toUpperCase():
            text = 'Fiabilité';
            break;
          case 'appDoneByCallOpByDayMoy'.toUpperCase():
            text = 'RDV tenu / jour';
            break;
          case 'caDoneMoyByMonth'.toUpperCase():
            text = 'CA tenu moyen / mois';
            break;
          case 'caChargedMoy'.toUpperCase():
            text = 'CA facturé moyen';
            break;
          case 'sellerTurnOverRate'.toUpperCase():
            text = 'Turn over des commerciaux';
            break;
          case 'nbSaleHandledForOneSaleCharged'.toUpperCase():
            text = 'RDV / vente facturée';
            break;
          case 'successRate'.toUpperCase():
            text = 'Réussite';
            break;
          case 'successRateGlobal'.toUpperCase():
            text = 'Réussite';
            break;
          case 'successRateBySellerAppProspect'.toUpperCase():
            text = 'Réussite sur RDV prospect';
            break;
          case 'successRateBySellerAppClient'.toUpperCase():
            text = 'Réussite sur RDV client';
            break;
          case 'saleCanceledRate'.toUpperCase():
            text = 'Vente annulée';
            break;
          case 'averageBasket'.toUpperCase():
            text = 'Panier moyen';
            break;
          case 'savRate'.toUpperCase():
            text = 'SAV';
            break;
          case 'techAppAchievedRate'.toUpperCase():
            text = "Accomplissement";
            break;
          case 'nbSellerAppHandledForOneSaleCharged'.toUpperCase():
            text = 'RDV traité / vente facturée';
            break;
          case 'saleFundingRate'.toUpperCase():
            text = 'Financement';
            break;
          case 'quantity'.toUpperCase():
            text = 'Quantité';
            break;
          // STATUS RDV
          case 'InProgress'.toUpperCase():
            text = 'En cours';
            break;
          case 'Canceled'.toUpperCase():
            text = 'Annulé';
            break;
          case 'Rescheduled'.toUpperCase():
            text = 'Reporté';
            break;
          case 'Absent'.toUpperCase():
            text = 'Absent';
            break;
          case 'Shelved'.toUpperCase():
            text = 'Sans suite';
            break;
          case 'ToBeContinued'.toUpperCase():
            text = 'Négoc.';
            break;
          case 'sale'.toUpperCase():
            text = 'Vente';
            break;
          // P&S
          case 'pands'.toUpperCase():
            text = 'P&S';
            break;

          default:
            text = val;
            break;
        }
      }

      return text;
    }
    return Filter;
  }

});
