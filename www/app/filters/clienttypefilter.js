define([
  'app'
], function (app) {
  'use strict';

  angular.module('app').filter('clienttypefilter', [clienttypefilter]);

  function clienttypefilter() {
    var Filter = function (val) {
      if (angular.isDefined(val)) {
        val = val.toUpperCase();
        var type = "";

        switch (val) {
          case 'client'.toUpperCase():
            type = 'Client';
            break;
          case 'prospect'.toUpperCase():
            type = 'Prospect';
            break;
          case 'retracted'.toUpperCase():
            type = 'Rétracté';
            break;
          default:
            type = val;
            break;
        }

        return type;
      } else {
        return val;
      }

    }
    return Filter;
  }
});
