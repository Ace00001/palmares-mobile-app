define([
  './clienttypefilter',
  './momenttext',
  './appointmentstatustext',
  './moneydisplay',
  './saledisplay',
  './servicedisplay',
  './annuledisplay',
  './roletext',
  './codetext',
  './statboxvaluedisplay',
  './numberdisplay',
  './photopath',
  './dayoffintervaldisplay'
]);
