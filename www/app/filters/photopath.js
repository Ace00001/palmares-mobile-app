define([
  'app'
], function (app) {
  'use strict';
  angular.module('app').filter('photopath', [photopath]);

  function photopath() {
    var Filter = function (val) {
      if (!val) {
        return val;
      } else {
        return '{0}/content/uploadedimages/{1}.jpeg'.format(app.API_URL, val);
      }
    }
    return Filter;
  }
});
