define([
  'app'
], function (app) {
  'use strict';

  angular.module('app').filter('appointmentstatustext', [appointmentstatustext]);

  function appointmentstatustext() {
    var Filter = function (val, arg) {
        var valToUpper = val.toUpperCase();
        var status = "";

        switch (valToUpper) {
          case 'confirmed'.toUpperCase():
            status = "Confirmé";
            break;
          case 'InProgress'.toUpperCase():
            status = "En cours";
            break;
          case 'sale'.toUpperCase():
            status = "Vente";
            break;
          case 'absent'.toUpperCase():
            status = "Absent";
            break;
          case 'ToBeContinued'.toUpperCase():
            status = "Négoc.";
            break;
          case 'Canceled'.toUpperCase():
            status = "Annulé";
            break;
          case 'Shelved'.toUpperCase():
            status = "Sans suite";
            break;
          case 'Rescheduled'.toUpperCase():
            status = "Reporté";
            break;
          // TECH APP STATUTS ONLY
          case 'Uncomplete'.toUpperCase():
            status = "Partiel";
            break;
          case 'Completed'.toUpperCase():
            status = "Honoré";
            break;

          default:
            status = valToUpper;
            break;
        }
        return status;
      }
    return Filter;
  }
});
