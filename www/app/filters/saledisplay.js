define([
  'app',
  'common/formatter'
], function (app, formatter) {
  'use strict';
  angular.module('app').filter('saledisplay', [saledisplay]);

  function saledisplay() {
    var Filter = function (val) {
      return formatter.saledisplay(val);
    }
    return Filter;
  }
});
