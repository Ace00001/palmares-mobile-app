define([
  'app'
], function (app) {
  'use strict';
  angular.module('app').filter('numberdisplay', [numberdisplay]);

  function numberdisplay() {
    var Filter = function (val, decimal) {
      return Number(val).toFixed(decimal ? decimal : 0);
    }
    return Filter;
  }
});
