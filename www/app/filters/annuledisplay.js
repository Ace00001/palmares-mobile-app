define([
  'app',
  'common/formatter'
], function (app, formatter) {
  'use strict';
  angular.module('app').filter('annuledisplay', [annuledisplay]);

		function annuledisplay() {
    var Filter = function (val) {
      return formatter.annuledisplay(val);
    }
    return Filter;
		}
});
