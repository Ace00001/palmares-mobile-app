define([
  'app',
  'ioc!branchService',
], function (app, branchService) {
  'use strict';

  var dependencies = ['$state', '$scope', '$stateParams', '$ionicScrollDelegate',
  '$ionicPopover', '$ionicLoading', 'pictureService'];
  angular.module('app')
    .controller('BranchFormController', dependencies.concat(BranchFormController));

  function BranchFormController($state, $scope, $stateParams, $ionicScrollDelegate,
  $ionicPopover, $ionicLoading, pictureService) {
    var vm = this;

    vm.viewTitle = 'Agence';
    vm.branchId = null;

    vm.branch = {
      name: '',
      phone: '',
      email: '',
      address: {
        text: null,
        latitude: 48.852063,
        longitude: 2.355841,
        street_number: '',
        route: '',
        postal_code: '',
        locality: '',
        country: ''
      },
      branchId: '00000000-0000-0000-0000-000000000000',
      photoId: '',
    };


    vm.isEditView = function () {
      return vm.branchId != '';
    }

    vm.isCreateView = function () {
      return !vm.isEditView();
    }


    vm.submit = function () {
      if (vm.isEditView()) {
        vm.branch.branchId = vm.branchId;
        branchService.putBranch(vm.branch)
          .done(function (r) {
            $state.go('tab.parameters');
          });
      } else {
        branchService.addBranch(vm.branch)
          .done(function (r) {
            $state.go('tab.parameters');
          });
      }
    };

    // ADDRESS
    // https://developers.google.com/maps/documentation/javascript/examples/places-autocomplete-addressform
    var componentForm = {
      street_number: 'short_name',
      route: 'long_name',
      postal_code: 'short_name',
      locality: 'long_name',
      country: 'long_name'
    };

    vm.locationchanged = function (it) {
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({ 'address': it }, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
          var place = results[0];
          vm.branch.address.longitude = place.geometry.location.lng();
          vm.branch.address.latitude = place.geometry.location.lat();

          // Get each component of the address from the place details
          // and fill the corresponding field on the form.
          for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
              var val = place.address_components[i][componentForm[addressType]];
              vm.branch.address[addressType] = val;
            }
          }
        } else {
          console.trace('Geocode was not successful for the following reason: ' + status);
        }
      });
    };

    function loadData() {
      vm.branchId = $stateParams.id;
      if (vm.isEditView()) {
        vm.viewTitle = "Modifier une agence";
        branchService.getBranch(vm.branchId)
          .done(function (r) {
            vm.branch = r;
          });
      } else {
        vm.viewTitle = "Ajouter une agence";
      }
    }

    vm.doRefresh = function () {
      loadData();
      $scope.$broadcast('scroll.refreshComplete');
    };

    loadData();

    //PHOTO ---------------------------------------------
    vm.imageData = null;
    vm.errors = []; // errors from pictureService

    var template = $('#popover-picture').html();
    vm.pop = $ionicPopover.fromTemplate(template, {
      scope: $scope
    });

    vm.showPicturePopover = function (e) {
      vm.pop.show(e);
    };

    vm.takePhoto = function () {
      vm.errors = [];
        pictureService.takePhoto()
          .then(function (imageData) {
            vm.imageData = imageData;
            vm.pop.hide();
          },
          function (error) {
            vm.errors.push(error);
          });
    };

    vm.selectPhoto = function () {
      vm.errors = [];
      pictureService.selectPhoto()
        .then(function (imageData) {
          vm.imageData = imageData;
          vm.pop.hide();
        },
        function (error) {
          vm.errors.push(error);
        });
    };

    vm.cancelPhoto = function () {
      vm.imageData = null;
      vm.errors = [];
      $ionicScrollDelegate.scrollTop();
    };

    vm.sendPhoto = function () {
      $ionicLoading.show({
        template: 'Chargement ...'
      });
      vm.errors = [];
      var args = {
        encoded: vm.imageData
      };
      branchService.sendPhoto(vm.branchId, args)
        .success(function (data) {
          vm.imageData = null;
          vm.status = 'Sent';
          loadData();
          $ionicLoading.hide();
        })
        .error(function (error) {
          if (error.message) {
            vm.errors.push(error.message);
            $ionicLoading.hide();
            return;
          }
          vm.errors.push('Une erreur s\'est produite.');
          $ionicLoading.hide();
        });
    };

  }

});

