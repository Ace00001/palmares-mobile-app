define([
  'app',
  'ioc!userService'
], function (app, userService) {
  'use strict';

  var dependencies = ['$state', '$scope', '$stateParams'];
  angular.module('app').controller('ManageController', dependencies.concat(ManageController));

  function ManageController($state, $scope, $stateParams) {
    var vm = this;

    vm.viewTitle = 'Gérer';
    vm.roleSet = ["director", "seller", "calloperator", "technician"];
    vm.searchText = '';
    vm.isSearchActive = false;
    vm.userProfileSet = [];

    vm.addUserProfile = function () {
      $state.go('tab.userprofile-form');
    }

    vm.showBranch = function () {
      var isCEO = app.inRole('ceo', 'director');
      return isCEO;
    }

    vm.activeSearch = function () {
      vm.isSearchActive = vm.searchText.length > 1;
    }

    vm.goToUserProfileCreate = function (userProfileId) {
      $state.go('tab.userprofile-form', { id: userProfileId });
    }

    function fetchdata() {
      userService.getUserSet().done(function (r) {
        vm.userProfileSet.length = 0;
        vm.userProfileSet.addRange(r);
        safeApply($scope);
      });
    }

    fetchdata();

  }

});

