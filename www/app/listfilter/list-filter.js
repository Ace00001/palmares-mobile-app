define([
  'app',
  'common/Messenger'
], function (app, mesenger) {
  'use strict';

  var dependencies = ['$ionicHistory', '$state'];

  function ListFilterController($ionicHistory, $state) {
    var vm = this;

    var backView = $ionicHistory.backView();
    var filterfor = $state.params.view;
    var multipleText = null;
    var multiple = null;
    var singleText = null;
    var single = null;
    var sorts = null;
    var title = null;
    var role = $state.params.param1;

    // FILTRES DISPONIBLES

    // CLIENT
    if (filterfor == "client-list") {
      title = 'FILTRE CLIENT';
      multipleText = "FILTRER PAR";
      multiple = ["prospect", "client", "retracted"];
      singleText = "DATE D’AJOUT";
      single = ["today", "thisweek", "thismonth", "nofilter"];
    } else if (filterfor == "appointment-list") {
      title = 'FILTRE RDV';
      multipleText = "FILTRER PAR";
      multiple = ["InProgress", "Canceled", "Rescheduled", "Absent", "Shelved", "ToBeContinued", "sale"];
      singleText = "DATE";
      single = ["today", "thisweek", "thismonth", "nofilter"];
    } else if (filterfor == "pands-list") {
      title = 'FILTRE P&S';
      multipleText = "FILTRER PAR";
      multiple = ["CA tenu", "CA facturé", "CA annulé", "Quantité vendue", "Négociation", "Ordre alphabétique"];
      singleText = "DATE";
      single = ["today", "thisweek", "thismonth", "nofilter"];
    } else if (filterfor == "sale-list") {
      title = 'FILTRE VENTE';
      multipleText = "FILTRER PAR";
      multiple = ["En attente", "Service prévu", "Service en cours", "Service accomplis", "Facturée", "Annulée"];
      singleText = "DATE";
      single = ["today", "thisweek", "thismonth", "nofilter"];
    } else if (filterfor == "profile") {
      title = 'FILTRE PROFIL';
      multipleText = "FILTRER PAR";
      singleText = "DATE";
      if (role == 'calloperator') {
        sorts = ["sellerAppDone", "reliabilityRate", "successRateGlobal"];
      } else if (role == 'seller') {
        sorts = ["caDone", "successRateBySellerAppProspect", "successRateBySellerAppClient", "saleCanceledRate", "averageBasket"]
      } else if (role == 'director') {
        sorts = ["caDone", "successRateBySellerAppProspect"];
      } else if (role == 'technician') {
        sorts = ["caAchieved", "quantity", "averageBasket"];
      } else {
        multiple = [];
      }

      single = ["today", "month", "year", "all"];
    }

    if (multiple) {
      multiple = multiple.select(function (it) {
        return {
          code: it,
          checked: false
        }
      });
    }

    if (single) {
      single = single.select(function (it) {
        return {
          code: it,
          checked: false
        }
      });
    }

    if (sorts) {
      sorts = sorts.select(function (it) {
        return {
          code: it
        }
      });
    }

    vm.submit = function () {
      $ionicHistory.goBack();
      mesenger.raise('listfilter.submit' + (role ? role : ''), {
        multiple: vm.multiple ? vm.multiple.where(function (it) {
          return it.checked;
        })
          .select(function (it) {
            return it.code
          }) : null,
        single: vm.singleChoice ? vm.singleChoice : null,
        sort: vm.sort ? vm.sort : null
      });
    };

    vm.singleChoice = null;
    vm.sort = null;
    vm.multiple = multiple;
    vm.multipleText = multipleText;
    vm.singleText = singleText;
    vm.single = single;
    vm.title = title;
    vm.sorts = sorts
  }

  angular.module('app').controller('ListFilterController', dependencies.concat(ListFilterController));
});
