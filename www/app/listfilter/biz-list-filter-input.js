define(['app', 'common/Messenger'], function (app, messenger) {
  angular.module('app').directive('bizListFilterInput', bizListFilterInput);

  function bizListFilterInput() {
    return {
      restrict: 'AE',
      templateUrl: 'app/listfilter/biz-list-filter-input.html',
      scope: {
        bizForView: '@',
        bizForTab: '@',
        bizParam1: '@',
        bizOnFilter: '=',
      },
      controller: ['$scope', '$state', '$timeout', bizListFilterInputController],
      controllerAs: 'vm'
    };
  }

  function bizListFilterInputController($scope, $state, $timeout) {
    var vm = this;

    vm._waitTime = null;
    vm.text = null;
    vm.criterias = null;
    vm.showFilter = true;

    // TODO : supprimer quand la recherche par critère fonctionne.
    // if ($scope.bizForView == "sale-list") {
    //   vm.showFilter = false;
    // }

    // $scope.preview = function(first){
    //   console.log(first);
    //   console.log('test');
    // }

    vm.gotoFilter = function () {
      console.log('gotoFilter')
      $state.go('tab.{0}-list-filter'.format($scope.bizForTab),
        { view: $scope.bizForView, param1: $scope.bizParam1 });
    };

    vm.setPartyMode = function ( ) {
      console.log('test');
    }

    vm.textchanged = function () {
      var that = this;
      this._waitTime = moment();
      var sensitive = 300;
      $timeout(function () {
        var now = moment();
        var duration = now.diff(that._waitTime);
        if (duration >= sensitive) {
          raise();
          that._waitTime = now;
        }
      }, sensitive);
    };

    function raise() {
      if ($scope.bizOnFilter) {
        $scope.bizOnFilter.call($scope.$parent.vm, {
          text: vm.text,
          criterias: vm.criterias
        })
      }
    }

    messenger.register('listfilter.submit' + ($scope.bizParam1 ? $scope.bizParam1 : ''))
      .done(function (r) {
        vm.criterias = r;
        raise();
      });

    $scope.$on('RESET_FILTER', function () {
      vm.text = "";
      vm.criterias = null;
    });
  }
});
